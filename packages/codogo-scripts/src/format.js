import runShell from "./runShell";

export default async ({ subcmd }) => {
	console.log("prettify gql");
	await runShell(
		"./node_modules/.bin/prettier",
		"--parser",
		"graphql",
		"--use-tabs",
		"--tab-width",
		"4",
		"--trailing-comma",
		"all",
		"--write",
		"src/**/*.graphql",
	);

	console.log("prettify js");
	await runShell(
		"./node_modules/.bin/prettier",
		"--use-tabs",
		"--tab-width",
		"4",
		"--trailing-comma",
		"all",
		"--write",
		"src/**/*.js",
	);

	console.log("eslint js");
	await runShell("./node_modules/.bin/eslint", "--color", "--fix", "src");

	if (subcmd === "add") {
		await runShell("git add -u");
	}
};
